﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace Selenium.Test.Media
{
    public class Configurations
    {
        public string URL { get; set; }
        public string SikuliPath { get; set; }

        public Configurations Read(string filePath)
        {
            var text = File.ReadAllText(filePath);

            var lines = text.Split('\n');

            var url = lines[0].Replace("\"", "").Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Replace(" ", "");
            var sikuliPath = lines[1].Replace("\"", "").Replace("\r\n", "").Replace("\r","").Replace("\n","").Replace(" ", "");

            url = url.Substring(url.IndexOf(':') + 1);
            sikuliPath = sikuliPath.Substring(sikuliPath.IndexOf(':') + 1);

            var config = new Configurations
            {
                URL = url,
                SikuliPath = sikuliPath
            };

            return config;
        }

    }
}
