﻿namespace Selenium.Test
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;

    /// <summary>
    /// The System.Diagnostics.Process class is tricky.  
    /// Sync reading blocks on the output or error 
    /// stream if the child process has not written 
    /// to it.  So if code reads output and there is nothing 
    /// there it blocks.  This is true of peek or any 
    /// other check on the stream.  If output is redirected 
    /// and the child process fills the pipe's buffer, the child 
    /// process locks up while writing.  What usually happens in 
    /// these cases is that both parent child are locked.
    /// Also WaitForExit(int) does not wait for async io to 
    /// complate nor does Kill.  If ExitCode is checked before 
    /// the process and all IO completes then an exception is 
    /// thrown.  Devs have to call WaitForExit() without the 
    /// timeout after calling Kill or WaitForExit(int).
    /// SafeProcess encapsulates the most common scenario we 
    /// use the Process class for in a safer interface that 
    /// takes into account the rules involved with redirecting output
    /// </summary>
    public class SafeProcess
    {
        /// <summary>
        /// The arguments.
        /// </summary>
        private readonly IEnumerable<string> arguments;

        /// <summary>
        /// The command.
        /// </summary>
        private readonly string command;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="SafeProcess"/> class.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        public SafeProcess(string command)
            :
                this(command, new string[0])
        {
        }

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="SafeProcess"/> class.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        public SafeProcess(string command, params string[] arguments)
            :
                this(command, (IEnumerable<string>)arguments)
        {
        }

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="SafeProcess"/> class.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        public SafeProcess(string command, IEnumerable<string> arguments)
        {
            if (String.IsNullOrEmpty(command))
            {
                throw new ArgumentNullException(
                    "command", "Command can not be null or empty");
            }

            if (arguments == null)
            {
                throw new ArgumentNullException("arguments");
            }

            if (arguments.Any(String.IsNullOrEmpty))
            {
                throw new ArgumentNullException(
                    "arguments", "Empty argument is not allowed");
            }

            this.command = command;
            this.arguments = arguments;
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="outputCallBack">
        /// The output call back.
        /// </param>
        /// <param name="millisecondTimeout">
        /// The millisecond Timeout.
        /// </param>
        /// <returns>
        /// The exit code.
        /// </returns>
        public int Execute(
            Action<string> outputCallBack, int millisecondTimeout)
        {

            return this.Execute(
                outputCallBack, outputCallBack, millisecondTimeout);
        }
        /// <summary>
        /// The execute method runs the process
        /// </summary>
        /// <param name="outputCallBack">
        /// The output call back.
        /// </param>
        /// <param name="errorCallback">
        /// The error callback.
        /// </param>
        /// <param name="millisecondTimeout">
        /// The millisecond Timeout.
        /// </param>
        /// <returns>
        /// The exit code
        /// </returns>
        public int Execute(
            Action<string> outputCallBack,
            Action<string> errorCallback,
            int millisecondTimeout)
        {
            if (outputCallBack == null)
            {
                throw new ArgumentNullException("outputCallBack");
            }

            if (errorCallback == null)
            {
                throw new ArgumentNullException("errorCallback");
            }
            
            using (Process process = new Process())
            {
                process.StartInfo.FileName = this.command;
                process.StartInfo.Arguments =
                    this.arguments.Aggregate(
                        (result, argument) => result + " " + argument);

                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.ErrorDialog = false;

                process.StartInfo.UseShellExecute = false;

                process.StartInfo.RedirectStandardOutput = true;
                process.OutputDataReceived +=
                    (sender, e) => outputCallBack(e.Data);

                process.StartInfo.RedirectStandardError = true;
                process.ErrorDataReceived +=
                    (sender, e) => errorCallback(e.Data);

                process.Start();
                process.BeginErrorReadLine();
                process.BeginOutputReadLine();

                if (!process.WaitForExit(millisecondTimeout))
                {
                    process.Kill();
                }

                process.WaitForExit();

                return process.ExitCode;
            }
        }
    }
}
