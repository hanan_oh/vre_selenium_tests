﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FireFoxTests.cs" company="">
//   
// </copyright>
// <summary>
//   The fire fox tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Selenium.Test
{         
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Reflection;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.Remote;
    using OpenQA.Selenium.Support.UI;
    using IniParser;
    using Models;
    using Selenium.Test.Media;
    using System.Diagnostics;
    using System.Collections.Generic;




    

/// <summary>
/// Before running these 
/// cmd 
/// cd "C:\Program Files (x86)\Mozilla Firefox"
/// firefox.exe  -p
/// create a new profile Test -- this test looks for this profile
/// close firefox and run it again as before select the new profile
/// http to the back end
/// load unity and select always run for the plugin when propted
/// close firefox 
/// you can repeat to cofirm unity auto loads now
/// </summary>
[TestClass]
    [DeploymentItem(@"Media\AeroMexico7PNG_trimme_.avi", "Media")]
    [DeploymentItem("Configurations.txt")]
    [DeploymentItem("geckodriver.exe")]
    public class FireFoxTests//unity3d
    {
        private RemoteWebDriver driver;

        private string nameString;


        private Configurations configurations;

        public object document { get; private set; }
        
        [TestInitialize]
        public void TestInitialize()
        {
            configurations = new Configurations();
            configurations = configurations.Read("Configurations.txt");

            nameString = "Selenium {0} " + DateTime.Now.ToString("yyyyMMdd_HH_mm_ss.ff");
            var options = new FirefoxOptions();

            string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            path = Path.Combine(path, @"Mozilla\Firefox");


            var parser = new FileIniDataParser();

            string profile = Path.Combine(path, "profiles.ini");
            var iniFile = parser.ReadFile(profile);

            var section = iniFile.Sections.FirstOrDefault(x => x.Keys.ContainsKey("Name") && string.Equals(x.Keys["Name"], "test", StringComparison.OrdinalIgnoreCase));
            path = Path.Combine(path, section.Keys["Path"]);



 //           FirefoxProfileManager profileM = new FirefoxProfileManager();
//            FirefoxProfile ffprofile = profileM.GetProfile("test");
            
 //           options.Profile = ffprofile;// new FirefoxProfile(path, false);

 //           options.Profile.SetPreference("plugins.testmode", true);
            
                driver = new FirefoxDriver(options)

                  {
                Url = configurations.URL,
                //sikuliPath = configurations.SikuliPath

            };
           LoginSynect();
        }


        [TestCleanup]
        public void TestCleanup()
        {
            // 
            //  driver.Quit();
        }

        //Find and click generic method:

        public void FindFillAndClick(string locator, string fieldValue, bool fieldExists, int waitInSec = 10)

        {
            ///Locates an Element on the Page by XPATH
            ///
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitInSec));
            IWebElement xpath = driver.FindElementByXPath(locator);

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(locator)));
            if (fieldExists)
            {
                xpath.SendKeys(fieldValue);
            }
            else
            {
                xpath.Click();
            }

        }


        /// The first test.
        /// Login as SynectAdmin
        
        public void LoginSynect()
        {
            
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Message = "Hello World!";
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("btn-primary")));
            Thread.Sleep(3000);
            driver.FindElementById("username").SendKeys("SynectAdmin");
            driver.FindElementById("password").SendKeys("SynectAdmin");
            driver.FindElementByClassName("btn-primary").Submit();

        }

        [TestMethod]
        /// Manage content
        /// 
        public void ManageContentAdminTest()
        {            
            String sikulipath = configurations.SikuliPath;

            var clientName = String.Format(nameString, "Client");
            var themeName = String.Format(nameString, "Theme");
            var conceptName = String.Format(nameString, "Concept");
            var layoutName = String.Format(nameString, "layout");
            var location = "MS Store 0";
            var monitorName ="Samsung 55''";
            var videowallName = "Left";
            var mediaName = String.Format(nameString, "media");
            String path = @"Media\AeroMexico7PNG_trimme_.avi";
            String command = @"C:\Program Files\sikuli\runsikulix.cmd -r" + sikulipath;

            path = Path.Combine(Path.GetDirectoryName(Assembly.GetAssembly(typeof(FireFoxTests)).Location), path);


            CreateAClient(clientName);

            CreateATheme(themeName);

            CreateAConcept(conceptName);

            CreateALayout(layoutName, clientName);

            CreateLocation(location);

            CreateMonitor(monitorName);

            CreateAVideowall(videowallName,clientName);

            uploadMedia(mediaName, themeName, clientName, conceptName, videowallName, location, path);

            LaunchReview();

            RunSikuli(command);
        }
        
        ///Create a client
        public void CreateAClient(string clientName)
            
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            while (!driver.Url.Contains("upload"))
            {
                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[@href='upload.html']")));

                wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("uploadsection")));
                driver.FindElementById("uploadsection").Click();
                

                wait.Until(ExpectedConditions.UrlContains("upload"));


                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[contains(@tooltip,'Create New Client')]")));
                var btn = driver.FindElementByXPath("//div[contains(@tooltip,'Create New Client')]");
                btn.Click();

                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//input[@class='field required']")));
                driver.FindElementByXPath("//input[@class='field required']").SendKeys(clientName);

                // Click the create button
                driver.FindElementByXPath("//div[@id='confirm']").Click();
            }




        }
        
        /// <summary>
        /// Create a Theme
        public void CreateATheme(string themeName)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            if (!driver.Url.Contains("upload"))
            {
                wait.Until(ExpectedConditions.ElementIsVisible(By.Id("uploadsection")));
                driver.FindElementById("uploadsection").Click();

            }

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@tooltip='Create New Theme']")));
            var btn = driver.FindElementByXPath("//div[@tooltip='Create New Theme']");
            btn.Click();
            driver.FindElementByXPath("//input[@class='field required']").SendKeys(themeName);

            //Handling popup window to browse

            string currentHandle = driver.CurrentWindowHandle;
            driver.FindElementByXPath("//input[@type='file']").SendKeys("C:\\DEV\\images\\admin.png");
            driver.FindElementByXPath("//div[@class='button confirm']").Click();

        }

        /// <summary>
        /// Create a Concept
        /// </summary>
        /// <param name="conceptName"></param>
     
        public void CreateAConcept(string conceptName)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            if (!driver.Url.Contains("upload"))
            {
                wait.Until(ExpectedConditions.ElementIsVisible(By.Id("uploadsection")));
                driver.FindElementById("uploadsection").Click();

            }

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@tooltip='Create New Concept']")));
            var btn = driver.FindElementByXPath("//div[@tooltip='Create New Concept']");
            btn.Click();
            driver.FindElementByXPath("//input[@class='field required']").SendKeys(conceptName);

            driver.FindElementByXPath("//div[@class='button confirm']").Click();
        }



        ///Edit Video Walls
        ///
        public void CreateALayout(string layout, string clientName)
        {
            //Click on Edit Videowall
            driver.FindElementByXPath("//a[@id='layoutsection']").Click();
            Thread.Sleep(3000);

            ///Select Selenium Client
            ///

            ///
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));

            
            Thread.Sleep(3000);

            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//select[@id='clientmenu']")));
            var dropDownElement = driver.FindElementByXPath("//select[@id='clientmenu']");

            string selected = dropDownElement.GetAttribute(clientName);

            //driver.FindElementByXPath("//select[@id='clientmenu']").Click();
            //driver.FindElementByXPath("//select[@id='clientmenu']").SendKeys(clientName);

            if (selected != clientName)

            {
                new SelectElement(dropDownElement).SelectByText(clientName);

            }

            else
            {

            }


            ///Create a selenium layout MS Store 0
            ///

            driver.FindElementByXPath("//div[@tooltip='Add New Layout']").Click();
            driver.FindElementByXPath("//input[@class='field required']").SendKeys("MS Store 0");
            driver.FindElementByXPath("//input[@class='field']").SendKeys("Selenium Layout Description");
            driver.FindElementByXPath("//div[@id='confirm']").Click();

        }
        /// Create Location
        ///
        public void CreateLocation(string location)

        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@defaulttooltip='Add New Location']")));
            driver.FindElementByXPath("//div[contains(@tooltip,'Add New Location')]").Click();
            driver.FindElementByXPath("//input[@class='field required']").SendKeys("MS Store 0");
            driver.FindElementByXPath("//input[@class='field']").SendKeys("Home Address");
            driver.FindElementByXPath("//div[@class='button confirm']").Click();


        }

        //Create a monitor
        
        public void CreateMonitor(string monitorName)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[@href='monitors.html']")));
            driver.FindElementByXPath("//a[@href='monitors.html']").Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("id('monitormenu')")));


            //Select "new monitor" from DropDown

            Thread.Sleep(3000);

            var dropDown = driver.FindElementByXPath("//select[@id='monitormenu']");

            Thread.Sleep(3000);

            string selection = dropDown.GetAttribute("New Monitor");

            Thread.Sleep(3000);
            if (selection != "New Monitor")
            {
                new SelectElement(dropDown).SelectByText("New Monitor");
            }
            



          




                //Brand
                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='brand']")));
            driver.FindElementByXPath("//input[@id='brand']").SendKeys("LG 55''");

            //Model
            driver.FindElementByXPath("//input[@id='model']").SendKeys("55''");

            //Width
            driver.FindElementByXPath("//input[@id='width']").SendKeys("1.2218");

            //Height
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//input[@id='height']")));

            driver.FindElementByXPath("//input[@id='height']").SendKeys("0.6926");

            //Diagnoal size
            driver.FindElementByXPath("//input[contains(@id,'size')]").SendKeys("55");

            //Bezel size
            driver.FindElementByXPath("//input[@id='bezel']").SendKeys("2000");

            //Surface Type Select "Glossy" From DropDown
            var dropDownElement2 = driver.FindElementByXPath("//select[contains(@id,'surface')]");

            dropDownElement2.Click();

            var surfaceType = "Glossy";

            driver.FindElementByXPath("//select[@id='surface']").SendKeys(surfaceType);

            //H.View Angle
            driver.FindElementByXPath("//input[@id='hviewangle']").SendKeys("70");

            //V.View Angle
            driver.FindElementByXPath("//input[@id='vviewangle']").SendKeys("70");

            driver.FindElementByXPath("//div[@id='savebutton']").Click();









        }
        //Create a VideoWall

        public void CreateAVideowall(string videoWallName,string clientName)
        {

            //Click on "Edit VideoWalls"

            driver.FindElementByXPath("//a[@id='layoutsection']").Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

            //Select Client

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//select[@id='clientmenu']")));

            var dropDownElement = driver.FindElementByXPath("//select[@id='clientmenu']");

            new SelectElement(dropDownElement).SelectByText(clientName);

            //click on "Add New VideoWall"

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@defaulttooltip='Add New Video Wall']")));
            driver.FindElementByXPath("//div[@defaulttooltip='Add New Video Wall']").Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//input[@data-type='string']")));

            driver.FindElementByXPath("//input[@data-type='string']").SendKeys(videoWallName);
            driver.FindElementByXPath("//input[@class='field']").SendKeys("Selenium Desc");

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='vwCols']")));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//input[@id='vwRows']")));

            

            driver.FindElementByXPath("//input[@id='vwCols']").Clear();
            driver.FindElementByXPath("//input[@id='vwCols']").SendKeys("44");
            driver.FindElementByXPath("//input[@id='vwRows']").Clear();
            driver.FindElementByXPath("//input[@id='vwRows']").SendKeys("1");

            
            //Confirm button By ID
            driver.FindElementByXPath("//div[@id='confirm']").Click();

            Thread.Sleep(5000);


        }

        //Manage Content

        public void uploadMedia(string media, string themeName, string clientName, string conceptName, string videoWall, string location, string path)

        {
            //Click on manage content

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//a[contains(@href,'upload.html')]")));
            driver.FindElementByXPath("//a[contains(@href,'upload.html')]").Click();

            Thread.Sleep(3000);

            //Select the selenium client
            //
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//select[@id='clientmenu']")));

            var dropDownElement = driver.FindElementByXPath("//select[@id='clientmenu']");

            new SelectElement(dropDownElement).SelectByText(clientName);

            var DropDownLocation = driver.FindElementByXPath("//select[@id='locationmenu']");


            new SelectElement(DropDownLocation).SelectByText(location);


            //Click on Choose MEDIA

            var btn = driver.FindElement(By.XPath("//div[@id='uploadbuttontext']"));
            btn.Click();
            wait.Until(ExpectedConditions.ElementIsVisible((By.XPath("//input[@class='field']"))));
            driver.FindElementByXPath("//input[@class='field']").SendKeys("1");

            //Drag And Drop a Video 

            driver.FindElementByXPath("//div[@id='box']//select[.='StoryboardMovie']//option[2]").Click();

            IWebElement droparea = driver.FindElement(By.XPath("//dragtext[@id='dragtext']"));
            
            DropFile(droparea, @path);
        }


        static void DropFile(IWebElement target, string filePath, int offsetX = 0, int offsetY = 0)
        {

            if (String.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentNullException("filePath");
            }

            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException(filePath);
            }



            IWebDriver driver = ((RemoteWebElement)target).WrappedDriver;
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));


            string JS_DROP_FILE = @"
        var target = arguments[0],
            offsetX = arguments[1],
            offsetY = arguments[2],
            document = target.ownerDocument || document,
            window = document.defaultView || window;

        var input = document.createElement('INPUT');
        input.type = 'file';
        input.style.display = 'none';
        input.onchange = function () {
          target.scrollIntoView(true);

          var rect = target.getBoundingClientRect(),
              x = rect.left + (offsetX || (rect.width >> 1)),
              y = rect.top + (offsetY || (rect.height >> 1)),
              dataTransfer = { files: this.files };

          ['dragenter', 'dragover', 'drop'].forEach(function (name) {
            var evt = document.createEvent('MouseEvent');
            evt.initMouseEvent(name, !0, !0, window, 0, 0, 0, x, y, !1, !1, !1, !1, 0, null);
            evt.dataTransfer = dataTransfer;
            target.dispatchEvent(evt);
          });

          setTimeout(function () { document.body.removeChild(input); }, 25);
        };
        document.body.appendChild(input);
        return input;
        ";

            IWebElement input = (IWebElement)jse.ExecuteScript(JS_DROP_FILE, target, offsetX, offsetY);
            input.SendKeys(filePath);
            wait.Until(ExpectedConditions.StalenessOf(input));

            driver.FindElement(By.XPath("//div[@class='button confirm']")).Click();



        }

        //Launch review 
        public void LaunchReview()
        {
            Uri url = new Uri(driver.Url);

            

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            Thread.Sleep(3000);
            


            while (driver.Url.Contains("upload"))
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//a[@id='startsection']")));
                driver.FindElementByXPath("//a[@id='startsection']").Click();
            }
               

            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("id('launchbutton')")));
            Thread.Sleep(4000);
            driver.FindElementByXPath("id('launchbutton')").Click();
        }


        public void RunSikuli(string command)
        {
            //configurations = new Configurations();
            //String path =configurations.SikuliPath;


            //SafeProcess process = new SafeProcess(@"c:\program files\sikuli\runsikulix.cmd","-r"+ path);

            //process.Execute(s => Debug.WriteLine(s), s => Debug.WriteLine(s), Timeout.Infinite);
            Thread.Sleep(55000);
            System.Diagnostics.Process.Start("c:\\bat\\1.bat");
        }
    }
}






