﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

class Program
{
    public static void Main()
    {
        Log.Logger = new LoggerConfiguration()
            .WriteTo.ColoredConsole()
            .WriteTo.Seq("http://localhost:5341")
            .CreateLogger();

        Log.Information("Hello, {Name}!", Environment.UserName);

        // Important to call at exit so that batched events are flushed.
        Log.CloseAndFlush();

        Console.ReadKey(true);
    }
}
