﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selenium.Test.Models
{
    public class Videowall
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ScreenConfiguration ScreenConfiguration { get; set; }
        public MonitorType MonitorType { get; set; }
    }
}
