﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selenium.Test.Models
{
    public enum ESurfaceType { GLOSSY, MATTE }

    public enum EOrientation { LANDSCAPE, PORTRAIT }

    public class MonitorType
    {
        public string Brand { get; set; }

        public string ModelName { get; set; }

        /// <summary>
        /// Height of the monitor, in meters
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// Width of the monitor, in meters
        /// </summary>
        public float Width { get; set; }

        /// <summary>
        /// Size in inches, for display and selection properties.
        /// </summary>
        public string Size { get; set; }

        /// <summary>
        /// Size in meters.
        /// </summary>
        public float BezelSize { get; set; }

        public ESurfaceType SurfaceType { get; set; }

        public int HorizontalViewAngle { get; set; }

        public int VerticalViewAngle { get; set; }
    }
}
