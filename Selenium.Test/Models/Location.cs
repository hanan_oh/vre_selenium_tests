﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selenium.Test.Models
{
    public class Location
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
