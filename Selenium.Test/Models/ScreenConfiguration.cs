﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selenium.Test.Models
{
    public class ScreenConfiguration
    {
        public EOrientation Orientation { get; set; }
        public int NumberOfRows { get; set; }
        public int NumberOfColumns { get; set; }
        public int VideoWallAngle { get; set; }
        public float DistanceFromFloor { get; set; }
    }
}
