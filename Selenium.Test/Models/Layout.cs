﻿

namespace Selenium.Test.Models
{
    public class Layout
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
